#include "Fact.hpp"
#include <stack>

#include <iostream>

Fact::Fact(char letter, FactsPtr facts) : _all_facts(facts), _letter(letter), _value(-1) { }

void  Fact::addRule(const std::pair<std::string, std::string> rule) {
  _rules.push_back(rule);
}

bool  Fact::solvePolish(const std::string& path, const std::string& rule, bool is_try) {
  std::stack<bool> tmp;

  unsigned i = 0;
  for (; i < rule.size(); ++i) {
    if (rule[i] == '!' || std::isupper(rule[i])) {
      bool neg = false;
      if (rule[i] == '!') {
        neg = true;
        ++i;
      }

      int  res;

      if (is_try) {
        if (rule[i] == _letter) {
          _value = neg ? 0 : 1;
          tmp.push((bool)_value);

        } else {

          auto search_res = (*_all_facts)->find(rule[i]);
          res = search_res->second.getValue(path);


          if (res == -1) {
            res = neg ? 0 : 1;
            tmp.push((bool)res);
          } else {
            if (neg)
              tmp.push((bool)!res);
            else
              tmp.push((bool)res);
          }
        }

        continue;
      }

      auto search_res = (*_all_facts)->find(rule[i]);
      res = search_res->second.getValue(path);

      // if fact can`t be solved -> set false
      if (res == -1) {
        search_res->second.setDefaultValue(0);
        res = 0;
      }

      if (neg)
        tmp.push((bool)!res);
      else
        tmp.push((bool)res);

    } else {
      bool f = tmp.top();
      tmp.pop();
      bool s = tmp.top();
      tmp.pop();


      switch (rule[i]) {
        case '+':
          tmp.push(f & s);
          break;
        case '|':
          tmp.push(f | s);
          break;
        case '^':
          tmp.push(f ^ s);
          break;
      }
    }
  }

  return tmp.top();
}


void  Fact::tryToSolveIt(const std::string& path) {

  for (const auto& rule : _rules) {
    bool res = solvePolish(path, rule.first, false);

    if (res == false) {
      continue;
    }

    res = solvePolish(path, rule.second, true);
    if (res == true) {
      return;
    }

    _value = -1;
  }
}

int Fact::getValue(const std::string& path) {

  if (_value == 1) {
    return _value;
  }

  if (_rules.empty()) {
    if (_value == -1)
      _value = 0;
    return _value;
  }

  if (path.find(_letter) != std::string::npos)
    return _value;

  std::string new_path = path;
  new_path.push_back(_letter);

  tryToSolveIt(new_path);

  if (_value == -1)
    _value = 0;

  return _value;
}

void Fact::setDefaultValue(int n) {
  _value = n;
}