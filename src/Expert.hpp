#ifndef EPERT_HPP
#define EPERT_HPP

#include "header.hpp"
#include "Fact.hpp"

class Expert {
public:
  Expert(const std::vector<Rule_and_fact>& rule_facts, const std::string& initial_facts, const std::string& queries);

  void setQueries(const std::string& queries);
  void setInitialFacts(const std::string& initial_facts);
  void resetInitialFacts(void);

  void solve(void);

private:
  FactsPtr    _factors;
  std::string _queries;
  std::string _initial_facts;
};

#endif //EPERT_HPP