#include "Expert.hpp"
#include <regex>

Expert::Expert(const std::vector<Rule_and_fact>& rule_facts, const std::string& initial_facts, const std::string& queries) {
  _factors = std::make_shared<Facts*>(new Facts);

  for (const auto& r_f : rule_facts) {
    for (const auto& letter : r_f.rule) {
      if (std::isupper(letter) && (*_factors)->find(letter) == (*_factors)->end()) {
        (*_factors)->insert({letter, Fact{letter, _factors} });
      }
    }

    for (const auto& letter : r_f.fact) {
      if (std::isupper(letter) && (*_factors)->find(letter) == (*_factors)->end()) {
        (*_factors)->insert({letter, Fact{letter, _factors} });
      }
    }

    for (const auto& letter : r_f.fact) {
      if (std::isupper(letter)) {
        auto founded = (*_factors)->find(letter);
        founded->second.addRule({r_f.rule, r_f.fact});
      }
    }

    if (r_f.biconditional) {
      for (const auto& letter : r_f.rule) {
        if (std::isupper(letter)) {
          auto founded = (*_factors)->find(letter);
          founded->second.addRule({r_f.fact, r_f.rule});
        }
      }
    }
  }

  setInitialFacts(initial_facts);
  setQueries(queries);
}

void Expert::setQueries(const std::string& queries) {
  if (!std::regex_match(queries, std::regex("^(\\?)([A-Z]*)(\\s)*$")))
    throw std::runtime_error("Syntax error into queries facts line");

  _queries = (queries.size() == 1) ? "" : queries.substr(1, queries.size() - 1);

  for (const char& letter : _initial_facts) {
    auto search_res = (*_factors)->find(letter);
    if (search_res == (*_factors)->end()) {
      std::string tmp;
      tmp.push_back(letter);
      throw std::runtime_error("Unknown queries fact: " + tmp);
    }
  }
}

void Expert::setInitialFacts(const std::string& initial_facts) {
  if (!std::regex_match(initial_facts, std::regex("^(=)([A-Z]*)(\\s)*$")))
    throw std::runtime_error("Syntax error into initital facts line");

  _initial_facts = (initial_facts.size() == 1) ? "" : initial_facts.substr(1, initial_facts.size() - 1);

  for (const char& letter : _initial_facts) {
    auto search_res = (*_factors)->find(letter);
    if (search_res != (*_factors)->end()) {
      search_res->second.setDefaultValue(1);
    } else {
      std::string tmp;
      tmp.push_back(letter);
      throw std::runtime_error("Unknown initital fact: " + tmp);
    }
  }
}

void Expert::resetInitialFacts(void) {
  for (auto& fact : *(*_factors)) {
      fact.second.setDefaultValue(-1);
  }
}

void Expert::solve(void) {
  if (_queries.empty())
    throw std::runtime_error("Nothing to solve - empty queries");

  for (const char& letter : _queries) {
      auto search_res = (*_factors)->find(letter);
      int val = search_res->second.getValue("");

      std::string res;
      switch (val) {
        case 1:
          res = GREEN "True" DEF;
          break;
        case 0:
          res = RED "False" DEF;
          break;
        case -1:
          res = YELLOW "Undetermined" DEF;
          break;
      }

      std::cout << CYAN << letter  << DEF << " is " << res << std::endl;
  }
}