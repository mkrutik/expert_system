#ifndef FACT_HPP
#define FACT_HPP

#include <vector>
#include <string>
#include <utility>
#include <map>
#include <memory>

class Fact;

typedef std::map<char, Fact> Facts;
typedef std::shared_ptr<Facts*> FactsPtr;

class Fact {

public:
  Fact(char letter, FactsPtr facts);

  void  addRule(const std::pair<std::string, std::string> rule);
  int   getValue(const std::string& path);

  void  setDefaultValue(int n);

private:
  void  tryToSolveIt(const std::string& path);
  bool  solvePolish(const std::string& path, const std::string& rule, bool is_try);

  typedef std::vector<std::pair<std::string, std::string> > Rules;

  FactsPtr    _all_facts;
  const char  _letter;
  int         _value;
  Rules       _rules;
};

#endif //FACT_HPP