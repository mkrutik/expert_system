#include "Fact.hpp"
#include "Expert.hpp"
#include "header.hpp"
#include <limits>

void print_info(void) {
  std::cout << std::endl 
            << CYAN << "Press " << RED << "\"C\"" << CYAN << " to close the program" << std::endl
            << "Press " << YELLOW << "\"I\"" << CYAN << " and then input new Initial Facts line" << std::endl
            << "Press " << GREEN << "\"Q\"" << CYAN << " and then input new queries line" << std::endl << std::endl;
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cout << RED << "Error: wrong number of arguments !" << DEF << std::endl
              << CYAN << "Usage: ./expert_system [file_name]" << DEF << std::endl;
  } else {
    try {
      std::string file_content = read_file(argv[1]);
      ParsedFile& res = prepare_data(file_content);

      Expert expert(res.rule_and_facts, res.initial_facts, res.queries);

      while (1) {
        std::cout << GREEN << "InitialFacts : " << DEF << res.initial_facts << std::endl
                  << GREEN << "Queries : " << DEF << res.queries << std::endl << std::endl; 

        expert.solve();
        expert.resetInitialFacts();

        print_info();

        std::string tmp;
        tmp.clear();

        char button;
        std::cin >> button;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        if (button == 'Q' || button == 'q') {
          std::cout << "Please write queries line and press enter (Must start with ? )" << std::endl;
          res.queries.clear();
          std::getline(std::cin, res.queries);
          expert.setQueries(res.queries);

        } else if (button == 'I' || button == 'i') {
          std::cout << "Please write Initial Facts line and press enter (Must start with = )" << std::endl;
          res.initial_facts.clear();
          std::getline(std::cin, res.initial_facts);
          expert.setInitialFacts(res.initial_facts);

        } else if (button == 'C' || button == 'c') {
          return 0;

        } else {
          std::cout << "Something unknonw was pressed, Good Buy!)";
          return 0;
        }

        std::cout << GREEN << "-------------------------------------------------------------------" << DEF << std::endl << std::endl;
      }

    } catch(const std::exception &e) {
      std::cout << RED << e.what() << DEF << std::endl;
    }
  }
}