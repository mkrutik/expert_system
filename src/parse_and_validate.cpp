#include "header.hpp"

#include <stack>
#include <iterator>
#include <streambuf>
#include <sstream>
#include <algorithm>
#include <regex>

std::vector<std::string> parse_data(const std::string& data) {
  std::vector<std::string> res;

  std::istringstream inss(data);
  std::string line;
  while (std::getline(inss, line))
  {
    if (line.empty())
      continue;

    // line contain only spaces
    std::string::iterator it = std::find_if( line.begin(), line.end(), [](char c ) {return !std::isspace(c);} );
    if (it == line.end())
      continue;

    size_t pos = line.find('#');
    if (pos == std::string::npos)
      res.push_back(line);
    else if (pos != 0) {
      std::string tmp = line.substr(0, pos);
      tmp.erase(std::remove_if(tmp.begin(), tmp.end(), isspace), tmp.end());
      if (!tmp.empty())
        res.push_back(tmp);

    }
  }

  return res;
}

std::string to_polish_view(const std::string& condition) {
  if (condition.empty())
    throw std::runtime_error("Polish view: empty argument");

  std::string str = condition;
  str.erase(std::remove_if(str.begin(), str.end(), isspace), str.end());

  std::string res;
  std::stack<char> tmp;

  for (unsigned i = 0; i < str.size(); ++i) {
    if (std::isupper(str.at(i))) {
      res.push_back(str.at(i));
    } else if (str.at(i) == '(') {
      tmp.push(str.at(i));
    } else if (str.at(i) == ')') {
      while (!tmp.empty() && tmp.top() != '(') {
        res.push_back(tmp.top());
        tmp.pop();
      }
      if (tmp.empty() || tmp.top() != '(')
        throw std::runtime_error("Invalid numbers of brackets");
      tmp.pop();
    } else if (str.at(i) == '!' && (i + 1) < str.size() && std::isupper(str.at(i + 1))) {
      res.push_back(str.at(i));
    } else if (str.at(i) == '+' && (i + 1) < str.size() &&
               (std::isupper(str.at(i + 1)) || str.at(i + 1) == '(' || (((i + 2) < str.size() && str.at(i+1) == '!' && std::isupper(str.at(i + 2)))))) {
      tmp.push(str.at(i));
    } else if (str.at(i) == '^' && (i + 1) < str.size() &&
              (std::isupper(str.at(i + 1)) || str.at(i + 1) == '(' || (((i + 2) < str.size() && str.at(i+1) == '!' && std::isupper(str.at(i + 2)))))) {
      while (!tmp.empty() && (tmp.top() == '+' || tmp.top() == '|')) {
        res.push_back(tmp.top());
        tmp.pop();
      }
      tmp.push(str.at(i));
    } else if (str.at(i) == '|' && (i + 1) < str.size() &&
              (std::isupper(str.at(i + 1)) || str.at(i + 1) == '(' || (((i + 2) < str.size() && str.at(i+1) == '!' && std::isupper(str.at(i + 2)))))) {
      while (!tmp.empty() && tmp.top() == '+') {
        res.push_back(tmp.top());
        tmp.pop();
      }
      tmp.push(str.at(i));
    } else {
      throw std::runtime_error("Syntax error in input");
    }
  }

  if (!tmp.empty()) {
    while (!tmp.empty() && tmp.top() != '(') {
      res.push_back(tmp.top());
      tmp.pop();
    }

    if (!tmp.empty() && tmp.top() == '(')
      throw std::runtime_error("Invalid numbers of brackets");
  }

  return res;
}


ParsedFile& prepare_data(const std::string& raw_data) {
  std::vector<std::string> parsed_data = parse_data(raw_data);

  if (parsed_data.empty()) {
    throw std::runtime_error("Invalid file content");
  }

  std::vector<Rule_and_fact> rule_and_facts;
  std::string initial_facts;
  std::string queries;

  for (const auto& line : parsed_data) {
    if (line.at(0) == INITIAL_SYMBOL) {
      if (!initial_facts.empty())
        throw std::runtime_error("Allowed only one line with initial facts");

      if (!std::regex_match(line, std::regex("^(=)([A-Z]*)(\\s)*$")))
        throw std::runtime_error("Syntax error into initial facts line");

      initial_facts = line;
    } else if (line.at(0) == QUERYES_SYMBOL) {
      if (!queries.empty())
        throw std::runtime_error("Allowed only one line with queries");

      if (!std::regex_match(line, std::regex("^(\\?)([A-Z]*)(\\s)*$")))
        throw std::runtime_error("Syntax error into queries facts line");

      queries = line;
    } else {
      Rule_and_fact rule_fact;

      size_t pos = line.find(BICONDITIONAL);
      if (pos != std::string::npos) {
        rule_fact.rule = to_polish_view(line.substr(0, pos));
        rule_fact.fact = to_polish_view(line.substr(pos + BICONDITIONAL.size(), line.size() - pos));
        rule_fact.biconditional = true;

        if (rule_fact.rule.empty() || rule_fact.fact.empty())
          throw std::runtime_error("Empty rule or fact");

        rule_and_facts.push_back(rule_fact);
        continue;
      }

        pos = line.find(CONDITIONAL);
        if (pos != std::string::npos) {
          rule_fact.rule = to_polish_view(line.substr(0, pos));
          rule_fact.fact = to_polish_view(line.substr(pos + CONDITIONAL.size(), line.size() - pos));
          rule_fact.biconditional = false;

          if (rule_fact.rule.empty() || rule_fact.fact.empty())
            throw std::runtime_error("Empty rule or fact");

          rule_and_facts.push_back(rule_fact);
          continue;
        }

        throw std::runtime_error("Syntax error: adsent condition symbol");
    }
  }

  if (initial_facts.empty())
    throw std::runtime_error("Line with initial facts required");

  if (queries.empty())
    throw std::runtime_error("Line with queries required");

  // remove spaces
  initial_facts.erase(std::remove_if(initial_facts.begin(), initial_facts.end(), isspace), initial_facts.end());
  queries.erase(std::remove_if(queries.begin(), queries.end(), isspace), queries.end());

  ParsedFile *res = new ParsedFile{rule_and_facts, initial_facts, queries};
  return *res;
}
