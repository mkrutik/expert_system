#include "header.hpp"

#include <fstream>

std::string read_file(const std::string& file_name) {
  try {
    std::ifstream file(file_name);

    if (!file.is_open())
      throw std::runtime_error("Error: can`t open the file " + file_name);

    std::string res((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    return res;

  } catch(std::exception& e) {
    throw std::runtime_error("Error: can`t open the file " + file_name);
  }
  return {};
}