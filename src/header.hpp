#ifndef HEADER_HPP
#define HEADER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <exception>

#define DEF     "\e[0;0m"
#define GREEN   "\e[1;32m"
#define RED     "\e[1;31m"
#define YELLOW  "\e[1;33m"
#define CYAN    "\e[0;35m"

typedef struct {
  std::string rule;
  std::string fact;
  bool biconditional;
} Rule_and_fact;

typedef struct {
  std::vector<Rule_and_fact> rule_and_facts;
  std::string initial_facts;
  std::string queries;
} ParsedFile;

const std::string CONDITIONAL = "=>";
const std::string BICONDITIONAL = "<=>";
const char INITIAL_SYMBOL = '=';
const char QUERYES_SYMBOL = '?';
const char COMMENT_SYMBOL = '#';

std::string read_file(const std::string& file_name);
ParsedFile& prepare_data(const std::string& raw_data);

#endif // HEADER_HPP